package com.aswdc.myapplication1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aswdc.myapplication1.R;
import com.aswdc.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvEmail = view1.findViewById(R.id.tvLstEmail);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);

        tvName.setText(userList.get(position).get(Const.FIRST_NAME) + " " + userList.get(position).get(Const.LAST_NAME));
        tvEmail.setText(String.valueOf(userList.get(position).get(Const.EMAIL_ADDRESS)));
        tvGender.setText(String.valueOf(userList.get(position).get(Const.GENDER)));
        return view1;
    }
}