package com.aswdc.myapplication1;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapplication1.database.MyDatabase;
import com.aswdc.myapplication1.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etEmail;
    ImageView ivClose;
    TextView tvDisplay;
    Button btnSubmit;
    ImageView ivBackground;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        intViewEvent();
        setTypefaceOnView();
        handleSavedInstance(savedInstanceState);
    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
    }

    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    String name = etFirstName.getText().toString();
                    String phoneNumber = etLastName.getText().toString();
                    String email = etEmail.getText().toString();
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetail(name, phoneNumber, email);
                    Toast.makeText(getApplicationContext(), lastInsertedId > 0 ?
                            "User Inserted Successfully" : "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* IMPLISIT INTENT */
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });
        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String abc = "some one has $192 for currency conversion @70";
                int intValueGetFromEditText;
                if (etLastName.getText().toString().length() > 0) {
                    try {
                        intValueGetFromEditText = Integer.parseInt(etLastName.getText().toString());
                    } catch (Exception e) {
                        intValueGetFromEditText = 0;
                    }
                }

            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
            }
        });
    }

    void initViewReference() {
        new MyDatabase(MainActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
        ivClose = findViewById(R.id.ivActClose);
        tvDisplay = findViewById(R.id.tvActDisplay);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbFootBall = findViewById(R.id.chbActFootBall);
        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);

        etLastName.setText("+91");

        getSupportActionBar().setTitle(R.string.lbl_main_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.home_screen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menuAboutUs) {
            Toast.makeText(MainActivity.this, "Menu Button Pressed", Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    boolean isValid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
        }

        //Phone Number Validation
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String phoneNumber = etLastName.getText().toString();
            if (phoneNumber.length() < 10) {
                etLastName.setError("Enter Valid Phone Number");
                flag = false;
            }
        }

        //Email Address Validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter Valid Email Address");
                flag = false;
            }
        }

        //CheckBox Validation
//        if (!(chbCricket.isChecked() && chbCricket.isChecked() && chbCricket.isChecked())) {
//            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
//            flag = false;
//        }

        return flag;
    }
}